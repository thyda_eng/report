# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


phnom_penh = Location.create(:name => 'Phnom Penh', :kind => 'Province')
kandal = Location.create(:name => 'Kandal', :kind => 'Province')
kompong_cham = Location.create(:name => 'Kompong Cham', :kind => 'Province')
siem_reap = Location.create(:name => 'Siem Reap', :kind => 'Province')
pursat = Location.create(:name => 'Pursat', :kind => 'Province')
kompong_speu = Location.create(:name => 'Kompong Speu', :kind => 'Province')
prey_veng = Location.create(:name => 'Prey Veng', :kind => 'Province')


khsach_kandal = Location.create(:name => 'Khsach Kandal', :parent_id => kandal.id, :kind => 'District')
Laveaem = Location.create(:name => 'Laveaem', :parent_id => kandal.id, :kind => 'District')
Keansvay = Location.create(:name => 'Keansvay', :parent_id => kandal.id, :kind => 'District')
mouk_kom_poul = Location.create(:name => 'Mouk Kom poul', :parent_id => kandal.id, :kind => 'District')

toul_kork = Location.create(:name => 'Toul Kork', :parent_id => phnom_penh.id, :kind => 'District')
sen_sok = Location.create(:name => 'Sen Sok', :parent_id => phnom_penh.id, :kind => 'District')
doun_penh = Location.create(:name => 'Doun Penh', :parent_id => phnom_penh.id, :kind => 'District')
reseykeo = Location.create(:name => 'Reseyko', :parent_id => phnom_penh.id, :kind => 'District')
toul_kork = Location.create(:name => 'Toul Kork', :parent_id => phnom_penh.id, :kind => 'District')

ta_ek = Location.create(:name => 'Taek', :parent_id => khsach_kandal.id, :kind => 'Commune')
khos_choram = Location.create(:name => 'Khos Choram', :parent_id => khsach_kandal.id, :kind => 'Commune')


toul_kork1 = Location.create(:name => 'Toul Kork I', :parent_id => toul_kork.id, :kind => 'Commune')
toul_kork2 = Location.create(:name => 'Toul Kork II', :parent_id => toul_kork.id, :kind => 'Commune')

reseykeo1 = Location.create(:name => 'Reseykeo I', :parent_id => reseykeo.id, :kind => 'Commune')
reseykeo2 = Location.create(:name => 'Reseykeo II', :parent_id => reseykeo.id, :kind => 'Commune')







