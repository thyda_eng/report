# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160712033524) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "diseases", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent_id"
    t.string   "kind"
  end

  create_table "pcr_forms", force: true do |t|
    t.string  "sample_id"
    t.string  "pcr_malaria"
    t.string  "tm"
    t.string  "rt_pcr_species"
    t.string  "k13"
    t.date    "date"
    t.string  "client_name"
    t.string  "sex"
    t.integer "age"
    t.string  "phone_number"
    t.string  "location"
    t.string  "latitude"
    t.string  "longitude"
    t.string  "province"
    t.string  "od"
    t.string  "hc"
    t.string  "commune"
    t.string  "village"
    t.date    "date_of_case_received"
    t.date    "date_of_k13_result"
    t.string  "final_k13_result"
    t.date    "date_final_k13_result"
    t.date    "dbs_received_date"
  end

  create_table "questions", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reports", force: true do |t|
    t.string   "sex"
    t.integer  "age"
    t.string   "longtitude"
    t.string   "latitude"
    t.integer  "disease_id"
    t.integer  "province_id"
    t.date     "created_at"
    t.datetime "updated_at"
    t.string   "phone_number"
    t.integer  "district_id"
    t.date     "date"
    t.integer  "user_id"
    t.integer  "village_id"
    t.integer  "commune_id"
    t.string   "suspective"
    t.integer  "baby_age"
    t.string   "treatment"
    t.string   "question"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "level"
    t.string   "password_hash"
    t.string   "phone"
    t.string   "supporter"
    t.integer  "location_id"
    t.string   "input_type"
  end

end
