class CreateTablePlaces < ActiveRecord::Migration
  def change
    create_table :table_places do |t|
      t.string :name
      t.string :type
      t.integer :parent_id, belongs_to: :places
      t.integer :survey_id, belongs_to: :surveys
    end
  end
end
