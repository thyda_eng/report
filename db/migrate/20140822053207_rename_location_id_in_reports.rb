class RenameLocationIdInReports < ActiveRecord::Migration
  def change
  	rename_column :reports, :location_id, :province_id
  end
end
