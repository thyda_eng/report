class AddUserIdToReports < ActiveRecord::Migration
  def change
  	add_column :reports, :user_id, :integer, belongs_to: :users
  end
end
