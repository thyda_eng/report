class AddFieldToReports < ActiveRecord::Migration
  def change
    add_column :reports, :village_id, :integer
    add_column :reports, :commune_id, :integer
    add_column :reports, :suspective, :string
    add_column :reports, :baby_age, :integer
    add_column :reports, :treatment, :string
  end
end
