class CreatePcrForms < ActiveRecord::Migration
  def change
    create_table :pcr_forms do |t|
      t.string :sample_id
      t.string :pcr_malaria
      t.string :tm
      t.string :rt_pcr_species
      t.string :k13
      t.date :date
      t.string :client_name
      t.string :sex
      t.integer :age
      t.string :phone_number
      t.string :location
      t.string :latitude
      t.string :longitude    	
    end
  end
end
