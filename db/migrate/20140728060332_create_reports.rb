class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :sex
      t.integer :age
      t.string :longtitude
      t.string :latitude
      t.belongs_to :disease
      t.belongs_to :location
      t.belongs_to :job

      t.timestamps
    end
  end
end
