class DropTable < ActiveRecord::Migration
  def change
  	drop_table :surveys
  	drop_table :places
  end
end
