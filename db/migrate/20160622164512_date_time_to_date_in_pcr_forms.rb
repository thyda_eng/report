class DateTimeToDateInPcrForms < ActiveRecord::Migration
  def change
    change_column :pcr_forms, :date, :date
    change_column :pcr_forms, :date_of_case_received, :date
    change_column :pcr_forms, :date_of_k13_result, :date
  end
end
