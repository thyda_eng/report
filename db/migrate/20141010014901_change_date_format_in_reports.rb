class ChangeDateFormatInReports < ActiveRecord::Migration
  def change
  	change_column :reports, :date, :datetime
  end
end
