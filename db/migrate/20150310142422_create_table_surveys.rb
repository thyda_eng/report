class CreateTableSurveys < ActiveRecord::Migration
  def change
    create_table :table_surveys do |t|
      t.integer :sample_id
      t.string :pcr_malaria
      t.string :tm
      t.string :rt_pcr_species
      t.string :k13
      t.date :date
      t.string :client_name
      t.string :sex
      t.integer :age
      t.string :phone_number
      t.integer :place_id, belongs_to: :places
      t.string :latitude
      t.string :longitude
    end
  end
end
