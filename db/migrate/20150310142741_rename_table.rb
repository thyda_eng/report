class RenameTable < ActiveRecord::Migration
  def change
  	rename_table :table_places, :places
  	rename_table :table_surveys, :surveys
  end
end
