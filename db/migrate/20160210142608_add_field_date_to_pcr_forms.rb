class AddFieldDateToPcrForms < ActiveRecord::Migration
  def change
    add_column :pcr_forms, :date_of_case_received, :datetime
    add_column :pcr_forms, :date_of_k13_result, :datetime
  end
end
