class AddHierarchyToLocations < ActiveRecord::Migration
  def change
  	add_column :locations, :parent_id, :integer
  	add_column :locations, :type, :string
  end
end
