class AddQuestionToReports < ActiveRecord::Migration
  def change
    add_column :reports, :question, :string
  end
end
