class AddLocationIdToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :location_id, :integer, belongs_to: :locations
  end
end
