class AddFieldToPcrForms < ActiveRecord::Migration
  def change
  	add_column :pcr_forms, :province, :string
  	add_column :pcr_forms, :od, :string
  	add_column :pcr_forms, :hc, :string
  	add_column :pcr_forms, :commune, :string
  	add_column :pcr_forms, :village, :string
  end
end
