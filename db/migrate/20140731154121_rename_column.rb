class RenameColumn < ActiveRecord::Migration
  def change
  	rename_column :locations, :type, :kind
  end
end
