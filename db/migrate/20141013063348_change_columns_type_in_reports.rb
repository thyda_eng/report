class ChangeColumnsTypeInReports < ActiveRecord::Migration
  def change
  	change_column :reports, :date, :date
  	change_column :reports, :created_at, :date
  end
end
