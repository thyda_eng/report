class AddFieldsToPcrForms < ActiveRecord::Migration
  def change
    add_column :pcr_forms, :final_k13_result, :string
    add_column :pcr_forms, :date_final_k13_result, :date
  end
end
