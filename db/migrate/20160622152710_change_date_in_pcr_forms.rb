class ChangeDateInPcrForms < ActiveRecord::Migration
  def change
    change_column :pcr_forms, :date, :datetime
  end
end
