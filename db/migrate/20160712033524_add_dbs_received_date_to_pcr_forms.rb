class AddDbsReceivedDateToPcrForms < ActiveRecord::Migration
  def change
    add_column :pcr_forms, :dbs_received_date, :date
  end
end
