class AddDistrictIdToReports < ActiveRecord::Migration
  def change
  	add_column :reports, :district_id, :integer, belongs_to: :locations
  end
end
