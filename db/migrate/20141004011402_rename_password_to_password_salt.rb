class RenamePasswordToPasswordSalt < ActiveRecord::Migration
  def change
  	rename_column :users, :password, :password_salt
  end
end
