json.array!(@reports) do |report|
  json.extract! report, :id, :sex, :age, :longtitude, :latitude, :disease, :province, :district, :phone_number, :date, :created_at, :user
  json.url report_url(report, format: :json)
end
