class Report < ActiveRecord::Base
	belongs_to :disease
	belongs_to :user
	belongs_to :province, :class_name => "Location"
	belongs_to :district, :class_name => "Location"
	belongs_to :commune, :class_name => "Location"
	belongs_to :village, :class_name => "Location"

	def self.to_csv(options = {})
		reports = Report.order("reports.id desc")
		question = Question.last.title if Question.last != nil
		columns = ["id", "sex", "age", "disease", "province", "district", "commune", "village", "phone_number", "suspective", "baby_age", "treatment", question, "latitude", "longtitude", "date", "submited_date", "reporter", "reporter_phone_number"]
	  CSV.generate(options) do |csv|
	    csv << columns
	    reports.each do |report|
	    	tmp = []
	    	tmp << report.id
	    	tmp << report.sex ? report.sex : ''
	    	tmp << report.age ? report.age : ''
	    	if !report.disease.nil?
	    		disease = report.disease.name
	    	end

	    	if !report.province.nil?
	    		province = report.province.name
	    	end

	    	if !report.district.nil?
	    		district = report.district.name
	    	end

	        if !report.commune.nil?
	          commune = report.commune.name
	        end
	        if !report.village.nil?
	          village = report.village.name
	        end

	    	if !report.user.nil?
	    		username = report.user.username
	    		user_phone = report.user.phone
	    	end

	    	tmp << disease
	    	tmp << province
	    	tmp << district
	    	tmp << commune
	    	tmp << village
	    	tmp << report.phone_number ? report.phone_number : ''
	    	tmp << report.suspective ? report.suspective : ''
	    	tmp << report.baby_age ? report.baby_age : ''
	    	tmp << report.treatment ? report.treatment : ''
	    	tmp << report.question ? report.question : ''
	    	tmp << report.latitude ? report.latitude : ''
	    	tmp << report.longtitude ? report.longtitude : ''
	    	tmp << report.date
	    	tmp << report.created_at
	    	tmp << username
	    	tmp << user_phone
	      csv << tmp
	    end
	  end
	end

end
