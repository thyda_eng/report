class PcrForm < ActiveRecord::Base
	validates :latitude , numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90, message: 'is incorrect' }, :if => :latitude?
	validates :longitude, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 ,  message: 'is incorrect'}, :if => :longitude?

	RT = ['P. falciparum', 'P. falciparum/P. vivax']
	PCR_MALARIA = ['Negative', 'Positive']

	def self.to_csv(user, options = {})
		pcrForms = PcrForm.order("pcr_forms.id desc")
		columns = ["sample_id", "dbs_received_date" ,"date_of_case_received", "pcr_malaria", "tm", "rt_pcr_species", "k13", "final_k13_result", "date_of_case_received", "date_final_k13_result", "date",  "client_name", "sex", 
			"age", "phone_number", "village", "commune", "health center", "opeartional district", "province", "latitude", "longitude"]
		CSV.generate(options) do |csv|
	    csv << columns
	    pcrForms.each do |pcrForm|
	      csv << pcrForm.attributes.values_at(*columns)
	    end
	  end
	end

end
