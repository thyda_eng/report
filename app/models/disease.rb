class Disease < ActiveRecord::Base
	has_many :reports, dependent: :destroy
	validates :name, :presence => true
  	validates :name, :uniqueness => true
end
