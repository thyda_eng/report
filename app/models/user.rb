class User < ActiveRecord::Base
  attr_accessor :password
  before_save :encrypt_password

  validates_confirmation_of :password
  validates :password, :presence => true, :on => :create
  validates :username, :presence => true
  validates :level, :presence => true
  validates :username, :uniqueness => true
  
  has_many :reports
  belongs_to :location

  INPUT_TYPE = {"1" => "Provincial", "2" => "IPC"}
 
  def first_entry?
    input_type == "1"
  end

  def second_entry?
    input_type == "2"
  end

  def self.authenticate(username, password)
    user = User.find_by_username(username)
    if !user.nil? && user.password_hash == Digest::MD5.hexdigest(password)
    	return user
    else
    	return nil
    end 
  end

  def type
    if level == 1
      "Admin"
    elsif level == 2
      "Master"
    else level = 3
      "Viewer"
    end
  end

  def encrypt_password
    if password.present?
      # self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = Digest::MD5.hexdigest(password)
      # BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end
