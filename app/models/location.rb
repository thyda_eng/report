class Location < ActiveRecord::Base
	has_many :locations, :foreign_key => 'province_id', :class_name => "Report", :dependent => :destroy
  has_many :locations, :foreign_key => 'district_id', :class_name => "Report", :dependent => :destroy
  has_many :locations, :foreign_key => 'commune_id', :class_name => "Report", :dependent => :destroy
	has_many :locations, :foreign_key => 'village_id', :class_name => "Report", :dependent => :destroy
	belongs_to :parent, :class_name => "Location"
  has_many :users

	validates :name, :kind, :presence => true


  def self.sub_location location
    locations = Location.where(:parent_id => location.id)
    if locations.length > 0
      children = []
      locations.each do |l|
        children = children + sub_location(l)
      end
      return [location] + children
    else
      return [location]
    end
  end
end
