$(function(){
  initialize();
  exportExcel();
  exportCsv();
  switchView();
});

function initialize() {
  lat = 11.562108;
  lng = 104.888535;

  latlng = new google.maps.LatLng(lat, lng);
  var mapOptions = {
      mapTypeId: 'roadmap',
      zoom: 7,
      center: latlng
  };
  map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
  map.setTilt(45);
}

function exportExcel(){
  $('#exportXls').on("click", function(){
    $('#formatFile').val('xls');
  });
}

function exportCsv(){
  $('#exportCsv').on("click", function(){
    $('#formatFile').val('csv');
  });
}

function switchView(){
  // console.log('switchView');
  $('.view').on("click", function(){
    if($('#table').is(":visible")){
      loadMarker();
      $('#table').hide();
      $('#iconMap').hide();
      $('#iconTable').show();
      $('div.map').show();
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    }else{
      $('#table').show();
      $('#iconTable').hide();
      $('#iconMap').show();
      $('div.map').hide();
    }
    return;
  });
}

function loadMarker(){
  $.get( "pcr_forms/markers", function( data ) {
    var infowindow = new google.maps.InfoWindow;
    var marker, i;
    for (var i in data) {
        // console.log("sample_id: " + data[i]["title"]);
        latlng = new google.maps.LatLng(data[i]["latitude"], data[i]["longitude"]);
        marker = new google.maps.Marker({
                 position: latlng,
                 map: map,
                 title: 'Hello World!',
                 icon: customMarkerIcon(data[i])
               });
         google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                  infowindow.setContent(buildMarker(data[i]));
                  infowindow.open(map, marker);
              }
         })(marker, i));
     }
  });
}

function customMarkerIcon(pcr_form){
  icon = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
  if(pcr_form["k13"] == 'C580Y' || pcr_form["k13"] == 'R539T' || pcr_form["k13"] == 'Y493H' || pcr_form["k13"] == 'I543T'){
    icon = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
  }else{
    icon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
  }
  return icon;
}
function buildMarker(pcr_form){
  info = "<table class='table'>"+
  "<thead>"+
    "<tr>"+
      "<th>Sample-ID</th>"+
      "<th>PCR-Malaria</th>"+
      "<th>tm</th>"+
      "<th>RT-PCR-Species</th>"+
      "<th>K13</th>"+
      "<th>Date</th>"+
      "<th>Client Name</th>"+
      "<th>Sex</th>"+
      "<th>Age</th>"+
      "<th>Phone number</th>"+
      "<th>Village</th>"+
      "<th>Commune</th>"+
      "<th>HC</th>"+
      "<th>OD</th>"+
      "<th>Province</th>"+
      "<th>Latitude</th>"+
      "<th>Longitude</th>"+
    "</tr>"+
  "</thead>"+
  "<tbody>"+
    "<tr>"+
      "<td>"+(pcr_form["sample_id"] ? pcr_form["sample_id"] : '')+"</td>"+
      "<td>"+(pcr_form["pcr_malaria"] ? pcr_form["pcr_malaria"] : '')+"</td>"+
      "<td>"+(pcr_form["tm"] ? pcr_form["tm"] : '')+"</td>"+
      "<td>"+(pcr_form["rt_pcr_species"] ? pcr_form["rt_pcr_species"] : '')+"</td>"+
      "<td>"+(pcr_form["k13"] ? pcr_form["k13"] : '')+"</td>"+
      "<td>"+(pcr_form["date"] ? pcr_form["date"] : '')+"</td>"+
      "<td>"+(pcr_form["client_name"] ? pcr_form["client_name"] : '')+"</td>"+
      "<td>"+(pcr_form["sex"] ? pcr_form["sex"] : '')+"</td>"+
      "<td>"+(pcr_form["age"] ? pcr_form["age"] : '')+"</td>"+
      "<td>"+(pcr_form["phone_number"] ? pcr_form["phone_number"] : '')+"</td>"+
      "<td>"+(pcr_form["village"] ? pcr_form["village"] : '')+"</td>"+
      "<td>"+(pcr_form["commune"] ? pcr_form["commune"] : '')+"</td>"+
      "<td>"+(pcr_form["hc"] ? pcr_form["hc"] : '')+"</td>"+
      "<td>"+(pcr_form["od"] ? pcr_form["od"] : '')+"</td>"+
      "<td>"+(pcr_form["province"] ? pcr_form["province"] : '')+"</td>"+
      "<td>"+(pcr_form["latitude"] ? pcr_form["latitude"] : '')+"</td>"+
      "<td>"+(pcr_form["longitude"] ? pcr_form["longitude"] : '')+"</td>"+
    "</tr>"+
  "</tbody>";
  return info;
}
