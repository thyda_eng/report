$(function(){

  $('#pcr_form_pcr_malaria').on('change', function(){
    $selector = $('#pcr_form_pcr_malaria').parents('.control-group');
    if($(this).val() == 'Negative')
      $selector.nextAll('.control-group').addClass('disabled');
    else
      $selector.nextAll('.control-group').removeClass('disabled');
  });

  $('#pcr_form_rt_pcr_species').on('change', function(){
    $selector = $('#pcr_form_rt_pcr_species').parents('.control-group');
    if($(this).val() == 'P. falciparum/P. vivax')
      $selector.nextAll('.control-group').addClass('disabled');
    else
      $selector.nextAll('.control-group').removeClass('disabled');
  });

});
