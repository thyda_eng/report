require "csv"
class PcrFormsController < ApplicationController
  load_and_authorize_resource :except => [:create, :update], param_method: :pcrForm_params
  before_action :set_pcrForm, only: [:show, :edit, :update, :destroy]
  def index
    @pcrForms = pcrForm_scope.paginate(page: params[:page], per_page: 15)
    @k13PositiveCase = @pcrForms.where(:k13 => ['C580Y']).count
    @totalRecord = pcrForm_scope.count
    @currentRecord = @pcrForms.count
    @redIndicator = @pcrForms.where(:k13 => ['C580Y', 'R539T', 'Y493H', 'I543T']).count
    @blueIndicator = @pcrForms.where.not(:k13 => ['C580Y', 'R539T', 'Y493H', 'I543T']).count
    @greenIndicator = @pcrForms.where(pcr_malaria: 'Positive').where.not(:k13 => ['C580Y', 'R539T', 'Y493H', 'I543T']).count
  end

  def markers
    render json: pcrForm_scope
  end

  def new
  	@pcrForm = PcrForm.new
  end

  def create
    @pcrForm = PcrForm.new(pcrForm_params)
    respond_to do |format|
      if @pcrForm.save
        format.html { redirect_to pcr_forms_path, notice: 'PcrForm was successfully created.' }
        format.json { render :show, status: :created, location: @pcrForm }
      else
        format.html { render :new }
        format.json { render json: @pcrForm.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit

  end

  def update
    page = params[:page] != "" ? params[:page] : 1
    respond_to do |format|
      if @pcrForm.update(pcrForm_params)
        format.html { redirect_to pcr_forms_path(page: page), notice: 'PcrForm was successfully updated.' }
        format.json { render :index, status: :ok, location: @pcrForm }
      else
        format.html { render :edit }
        format.json { render json: @pcrForm.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @pcrForm.destroy
    respond_to do |format|
      format.html { redirect_to pcr_forms_url, notice: 'PcrForm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def export
    if params[:fromDate] != "" && params[:endDate] != ""
      @pcrForms = pcrForm_scope.where(:date => params[:fromDate]..params[:toDate])
    else
      @pcrForms = pcrForm_scope
    end

    respond_to do |format|
      format.csv { send_data @pcrForms.to_csv(current_user),:type => 'text/csv; charset=iso-8859-1; header=present',:disposition => "attachment; filename = export.csv" }
      format.xls do
        response.headers['Content-Disposition'] = 'attachment; filename="export.xls"'
        render "export.xls.erb"
      end
    end
  end

  # def sample_list

  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pcrForm
      @pcrForm = PcrForm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pcrForm_params
      params.require(:pcr_form).permit(:date_of_case_received, :date_of_k13_result, :sample_id, :pcr_malaria, :tm, :rt_pcr_species, :k13, :final_k13_result, :date_final_k13_result, :date, :client_name, :sex, :age, :phone_number, :location, :village, :commune, :hc, :od, :province, :latitude, :longitude)
    end

    def pcrForm_scope
      pcr_forms = PcrForm.order("date_of_case_received desc")
      pcr_forms = pcr_forms.where('lower(sample_id) = ?', params[:sample_id].downcase) if params[:sample_id].present? && params[:sample_id] != ""
      pcr_forms = pcr_forms.where(k13: params[:k13]) if params[:k13] && params[:k13] != "All"
      pcr_forms
    end

end
