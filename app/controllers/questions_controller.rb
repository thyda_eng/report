class QuestionsController < ApplicationController
  load_and_authorize_resource param_method: :question_params
  before_action :set_question, only: [:show, :edit, :update, :destroy]	
  skip_before_filter :check_authentication, :only => [:index, :last_question]

  def index
  	@questions = Question.all
  end

  def last_question
  	render json: Question.last
  end

  def new
  	@question = Question.new
  end

  def create
  	@question = Question.new(question_params)
    respond_to do |format|
      if @question.save
        format.html { redirect_to :questions, notice: 'Location was successfully created.' }
      end
    end
  end

  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to :questions, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_question
      @question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:title)
    end  
end