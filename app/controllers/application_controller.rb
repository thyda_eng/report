class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_request_header
  helper_method :after_login_path, :current_user, :admin_user, :pfd_user, :pfd_viewer_user, :pfd_data_entry_user, :report_user, :report_master_user, :report_viewer_user

  before_filter :check_authentication
  after_filter :store_location

  before_filter do
    resource = controller_path.singularize.gsub('/', '_').to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to session[:previous_url], :alert => exception.message
  end

  def after_login_path
    session[:previous_url] = request.fullpath
  end

  def store_location
    return unless request.get?
    if (request.path != "/sessions/new" &&
        request.path != "/sessions/destroy" &&
        !request.xhr?)
      session[:previous_url] = request.fullpath
    end
  end

  def set_request_header
    headers['Access-Control-Allow-Origin'] = '*'
  end

  def check_authentication
    unless current_user
      flash[:message] = 'You are not authenticate to do this action, please login!'
      return redirect_to login_path
    end
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def admin_user
    if current_user && (current_user.level == 1)
      true
    else
      false
    end
  end

  def pfd_user
    if current_user && (current_user.supporter == 'PFD')
      true
    else
      false
    end
  end

  def pfd_data_entry_user
    if pfd_user && current_user.level == 4
      true
    else
      false
    end
  end

  def pfd_viewer_user
    if pfd_user && current_user.level == 3
      true
    else
      false
    end
  end

  def report_user
    if current_user && (current_user.supporter == 'Other')
      true
    else
      false
    end
  end

  def report_master_user
    if report_user && current_user.level == 2
      true
    else
      false
    end
  end

  def report_viewer_user
    if report_user && current_user.level == 3
      true
    else
      false
    end
  end

  def is_admin?
    unless current_user.level == 1
      return redirect_to login_path
    end
  end

  def is_master?
    unless current_user.level == 2
      return redirect_to login_path
    end
  end

  def is_viewer?
    unless current_user.level == 3
      return redirect_to login_path
    end
  end

  def is_data_entry?
    unless current_user.level == 4
      return redirect_to login_path
    end
  end

end
