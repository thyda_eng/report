class LocationsController < ApplicationController
  load_and_authorize_resource :except => [:create, :update], param_method: :location_params
  before_action :set_location, only: [:show, :edit, :update, :destroy]
  skip_before_filter :check_authentication, :only => [:index, :parent_locations_with_kind, :parent_location_with_child_id, :sub_locations, :all_provinces]
  # GET /locations
  # GET /locations.json
  def index
    @locations = Location.order(:name)
  end

  # GET /locations/1
  # GET /locations/1.json
  def show
  end

  # GET /locations/new
  def new
    @location = Location.new
  end

  # GET /locations/1/edit
  def edit
  end

  # POST /locations
  # POST /locations.json
  def create
    if location_params[:kind] == 'Province'
      parent_id = nil
    elsif location_params[:kind] == 'District'
      parent_id = location_params[:province]
    elsif location_params[:kind] == 'Commune'
      parent_id = location_params[:district]
    else
      parent_id = location_params[:commune]
    end

    @location = Location.new(name: location_params[:name], kind: location_params[:kind], parent_id: parent_id)

    respond_to do |format|
      if @location.save
        format.html { redirect_to @location, notice: 'Location was successfully created.' }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /locations/1
  # PATCH/PUT /locations/1.json
  def update
    if location_params[:kind] == 'Province'
      parent_id = nil
    elsif location_params[:kind] == 'District'
      parent_id = location_params[:province]
    elsif location_params[:kind] == 'Commune'
      parent_id = location_params[:district]
    else
      parent_id = location_params[:commune]
    end 

    respond_to do |format|
      if @location.update(name: location_params[:name], kind: location_params[:kind], parent_id: parent_id)
        format.html { redirect_to @location, notice: 'Location was successfully updated.' }
        format.json { render :show, status: :ok, location: @location }
      else
        format.html { render :edit }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url, notice: 'Location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def parent_locations_with_kind
    kind = params[:kind]
    location_id = params[:location_id]

    # p kind
    @location = Location.new
    if kind == 'District'
      locations = Location.where(:kind => 'Province')
    elsif kind == 'Commune'
      locations = Location.where(:kind => 'District')
    else
      locations = nil
    end
      
    respond_to do |format|
      format.json { render json: locations }
    end

  end

  def parent_location_with_child_id
    location_id = params[:location_id]

    # p kind
    @location = Location.new
    location = Location.find(location_id).parent
      
    respond_to do |format|
      format.json { render json: location }
    end

  end



  def sub_locations
    location_id = params[:location_id]
    locations =  Location.where(:parent_id => location_id)
    respond_to do |format|
      format.json {render json: locations}
    end
  end

  def all_provinces
    locations = Location.where(:kind => 'Province')
    respond_to do |format|
      format.json {render json: locations}
    end    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:name, :province, :district, :kind, :commune)
    end
    def location_scope
      Location.order(:name)
    end

end
