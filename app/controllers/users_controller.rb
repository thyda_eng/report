class UsersController < ApplicationController
	load_and_authorize_resource  :except => [:index]
  skip_before_filter :check_authentication, :only => [:index]

  def index
    @users = user_scope.paginate(page: params[:page], per_page: 15)
    # User.all
    respond_to do |format|
      format.html
      format.json { render json: @users }
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :index, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
  	@user = User.new
    @locations = Location.all
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation, :level, :phone, :supporter, :location_id, :input_type)
    end

  def user_scope
    if admin_user
      users= User.all
    else
      users = User.where(:supporter => current_user.supporter)
    end
    current_user ? users.where.not(id: current_user.id) : users
  end
end
