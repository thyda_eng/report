require "csv"
class ReportsController < ApplicationController
  load_and_authorize_resource param_method: :report_params
  before_action :set_report, only: [:show, :edit, :update, :destroy]
  protect_from_forgery :except => [:create, :report_params]

  skip_before_filter :check_authentication, :only => [:create, :question]

  # GET /reports
  # GET /reports.json
  def index
    @question = Question.last
    @reports = report_scope.paginate(page: params[:page], per_page: 15)
  end

  def question
    render json: Settings.question
  end
  # GET /reports/1
  # GET /reports/1.json
  def show
  end

  # GET /reports/new
  def new
    @report = Report.new
  end

  # GET /reports/1/edit
  def edit
  end

  # POST /reports
  # POST /reports.json
  def create
    @report = Report.new sex: params[:sex], age: params[:age], disease_id: params[:disease_id],
     province_id: params[:province_id], district_id: params[:district_id], phone_number: params[:phone_number],
     latitude: params[:latitude], longtitude: params[:longtitude], date: params[:date], user_id: params[:user_id],
     village_id: params[:village_id], commune_id: params[:commune_id], suspective: params[:suspective],
     baby_age: params[:baby_age], treatment: params[:treatment], question: params[:question]

    respond_to do |format|
      if User.find(params[:user_id]) && @report.save
        format.html { redirect_to reports_path, notice: 'Report was successfully created.' }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reports/1
  # PATCH/PUT /reports/1.json
  def update
    respond_to do |format|
      if @report.update(report_params)
        format.html { redirect_to reports_path, notice: 'Report was successfully updated.' }
        format.json { render :show, status: :ok, location: @report }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to reports_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def export
    if params[:fromDate] != "" && params[:endDate] != ""
      @reports = Report.order("reports.id desc").where(:created_at => params[:fromDate]..params[:toDate])
    else
      @reports = Report.order("reports.id desc")
    end

    respond_to do |format|
      format.csv { send_data @reports.to_csv,:type => 'text/csv; charset=iso-8859-1; header=present',:disposition => "attachment; filename = export.csv" }
      format.xls do
        response.headers['Content-Disposition'] = 'attachment; filename="export.xls"'
        render "export.xls.erb"
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_params
      params.require(:report).permit(:sex, :age, :disease_id, :province_id, :district_id, :phone_number, :longtitude, :latitude, :date, :user_id, :village_id, :commune_id, :suspective, :baby_age, :treatment, :question)
    end

    def report_scope
      Report.order("reports.id desc")
    end

end
