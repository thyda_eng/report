class ImportController < ApplicationController
  protect_from_forgery :except => [:import]
  skip_before_filter :check_authentication


  def import
    csv_string = File.read(params[:file].path, :encoding => 'utf-8')
    PcrForm.transaction do
      csv = CSV.new csv_string, return_headers: false
      csv.each do |row|
        p row[0]
        result = PcrForm.find_by(sample_id: row[0])
        if result
          result.update_attributes(dbs_received_date: row[1], k13: row[2], date_of_k13_result: row[3], final_k13_result: row[4], date_final_k13_result: row[5])
          p '+++++++++++++++++++++++++++++++++++'
        end
      end
    end
  end
end
