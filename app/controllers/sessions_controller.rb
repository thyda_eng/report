class SessionsController < ApplicationController
  skip_before_filter :check_authentication

  def new
    if current_user
      redirect_to after_login_path
    end
  end

  def create
    if user = User.authenticate(params[:username], params[:password])
      session[:user_id] = user.id
      if user.supporter == 'PFD'
        if user.level == 4
          redirect_to  new_pcr_form_path
        else
          redirect_to  pcr_forms_path
        end
      else
        redirect_to  reports_path
      end
    else
      flash["message"] = "Username or password you entered is incorrect."
      render :new
    end

  end

  def destroy
    session[:user_id] = nil
    redirect_to login_url
  end
end
